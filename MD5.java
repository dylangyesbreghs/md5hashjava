package hash;

import java.math.*;
import java.security.*;

/**
 * Class MD5
 * @author Dylan Gyesbreghs
 * @version v0.1
 *
 */

public class MD5 {
	/**
	 * 
	 * @param passWord
	 * @return The hashed version of the password param
	 */
	public static String getHash(String passWord){
		String hash = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(passWord.getBytes(), 0, passWord.length());
			hash = new BigInteger(1, md.digest()).toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return hash;
	}
}